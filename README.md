# Ways you can donate to me!

The following repository and document is dedicated to listing all the payment methods I accept as donations.


## Referral Links

Support me by clicking the following referral links for services I use:

[![DigitalOcean Referral Badge](https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%203.svg)](https://www.digitalocean.com/?refcode=71f8848cef03&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)

- Hover: [https://hover.com/y5j6iyEp](https://hover.com/y5j6iyEp)

---

## Paypal

Check out my [Paypal.me](https://www.paypal.me/delta1512) link.

---

## Cryptocurrency

|Coin        |Address                                    |QR                      |
|------------|:------------------------------------------|:-----------------------|
|Gridcoin    |SDeLtAzzaNkvom9HzVgdHHGToEjZ7sYipp         |![](./QRs/Gridcoin.png) |
|Bitcoin     |1HrMV9fTaBGez2VkEGPoZe3ZnqcqdwHWJq         |![](./QRs/Bitcoin.png)  |
|Litecoin    |LUgVZeM8Qm8QK5Wtk7mcLyrMTfdinVXi4a         |![](./QRs/Litecoin.png) |
|ETH/ERC-20  |0x022651a913bf752773b85A34B8Bb84240bDDB14f |![](./QRs/ETH.png)      |
|BCH         |qpjxadk89nx0tk3gj4gwxlrxq07m04wlwvq3qts6pk |![](./QRs/BCH.png)      |
|BTG         |btg1qxhmkdaqr6xvdg8e09lq8j3llwu69wzxd465693|![](./QRs/BTG.png)      |
|DASH        |XivYi59cfigs1nfxfgpBMgYyLFw3wUqcj6         |![](./QRs/DASH.png)     |
|DGB         |dgb1qwee08enc9h5qgqpk5sy3dgs7ksehtawmv9c75h|![](./QRs/DGB.png)      |
|DOGE        |DQizdhYE1ofcKz1WtLsJeQ2gq6j6NX6SUH         |![](./QRs/DOGE.png)     |
|GRS         |grs1qrxsmgnszwcm5378ednnm6a0e078qpsr3ah8czs|![](./QRs/GRS.png)      |
|PPC         |PDt5oi9H68CyABWyoNpibSAcTVhbx1ku7e         |![](./QRs/PPC.png)      |
|RDD         |Ro1qbkpoPqGKxvjFJkMBVKYQ5gP4KVFNBy         |![](./QRs/RDD.png)      |
|SLR         |8RSymticfq3USYHF2UtmkHY2iz9cB14Eri         |![](./QRs/SLR.png)      |
